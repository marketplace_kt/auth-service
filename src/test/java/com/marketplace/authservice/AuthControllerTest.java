package com.marketplace.authservice;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketplace.authservice.config.jwt.JWTProvider;
import com.marketplace.authservice.dto.AuthRequestDto;
import com.marketplace.authservice.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;

import static com.marketplace.authservice.entity.RoleEnum.SELLER;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthControllerTest {

    @MockBean
    private AuthenticationManager authenticationManager;

    @MockBean
    private UserService userService;

    @MockBean
    private JWTProvider jwtProvider;

    @Autowired
    private MockMvc mvc;


    @Test
    public void whenUsernameAlreadyTaken_thenReturnBadRequest() throws Exception {
        AuthRequestDto registerDto = new AuthRequestDto("username", "password", SELLER);
        when(userService.existsByUsername(registerDto)).thenReturn(true);

        mvc.perform(post("/api/auth/sign-in")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(registerDto)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("User already exists"));
    }

    @Test
    public void whenUserIsSuccessfullyRegistered_thenReturnOk() throws Exception {
        AuthRequestDto registerDto = new AuthRequestDto("username", "password", SELLER);
        when(userService.existsByUsername(registerDto)).thenReturn(false);

        mvc.perform(post("/api/auth/sign-in")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(registerDto)))
                .andExpect(status().isOk())
                .andExpect(content().string("User successfully registered"));

        verify(userService).register(registerDto);
    }

    @Test
    public void whenUserIsSuccessfullyLoggedIn_thenReturnOk() throws Exception {
        AuthRequestDto loginDto = new AuthRequestDto("username", "password", SELLER);
        Authentication authentication = mock(Authentication.class);
        when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class))).thenReturn(authentication);
        when(jwtProvider.generateToken(authentication)).thenReturn("token");

        mvc.perform(post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(loginDto)))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"accessToken\":\"token\"}"));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
