package com.marketplace.authservice;

import com.marketplace.authservice.config.jwt.JWTProvider;
import com.marketplace.authservice.config.security.SecurityConstants;
import com.marketplace.authservice.dto.AuthRequestDto;
import com.marketplace.authservice.entity.RoleEnum;
import com.marketplace.authservice.entity.UserEntity;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;


import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
public class JWTProviderTest {


    @Autowired
    private JWTProvider jwtProvider;
    private UserEntity user;
    private Authentication authentication;

    @BeforeEach
    void setup() {
        user = new UserEntity();
        user.setUsername("username");
        user.setPassword("password");
        user.setRole(RoleEnum.SELLER);
        authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());
    }


    @Test
    void generateTokenTest() {
        String token = jwtProvider.generateToken(authentication);
        assertNotNull(token);

        Claims claims = Jwts.parserBuilder()
                .setSigningKey(SecurityConstants.JWT_SECRET)
                .build()
                .parseClaimsJws(token)
                .getBody();
        assertEquals("username", claims.getSubject());
    }

    @Test
    void getUsernameFromJWTTest() {
        String token = jwtProvider.generateToken(authentication);
        assertNotNull(token);

        String username = jwtProvider.getUsernameFromJWT(token);
        assertEquals("username", username);
    }

    @Test
    void validateTokenTest() {
        String token = jwtProvider.generateToken(authentication);
        assertNotNull(token);
        assertTrue(jwtProvider.validateToken(token));

        String invalidToken = "invalidToken";
        assertThrows(AuthenticationCredentialsNotFoundException.class, () -> jwtProvider.validateToken(invalidToken));
    }
}
