package com.marketplace.authservice;

import com.marketplace.authservice.config.jwt.JWTAuthenticationFilter;
import com.marketplace.authservice.config.jwt.JWTProvider;
import com.marketplace.authservice.dto.AuthRequestDto;
import com.marketplace.authservice.entity.UserEntity;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.marketplace.authservice.service.UserService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.lang.reflect.Method;

import static com.marketplace.authservice.entity.RoleEnum.SELLER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
public class JWTAuthenticationFilterTest {

    private final JWTAuthenticationFilter jwtAuthenticationFilter;

    private final JWTProvider jwtProvider;

    private final UserService userService;

    @Autowired
    public JWTAuthenticationFilterTest(JWTAuthenticationFilter jwtAuthenticationFilter, JWTProvider jwtProvider, UserService userService) {
        this.jwtAuthenticationFilter = jwtAuthenticationFilter;
        this.jwtProvider = jwtProvider;
        this.userService = userService;
    }

    private UserEntity user;

    private Authentication authentication;

    @BeforeEach
    void setup() {
        user = new UserEntity();
        user.setUsername("username");
        user.setPassword("password");
        user.setRole(SELLER);
        userService.register(new AuthRequestDto(user.getUsername(), user.getPassword(), user.getRole()));
        authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());
    }


    @Test
    void getJWTFromRequestTest() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader("Authorization")).thenReturn("Bearer jwtToken");
        Method getJWTFromRequest = JWTAuthenticationFilter.class.getDeclaredMethod("getJWTFromRequest", HttpServletRequest.class);
        getJWTFromRequest.setAccessible(true);
        String jwt = (String) getJWTFromRequest.invoke(jwtAuthenticationFilter, request);
        assertEquals("jwtToken", jwt);

    }

    @Test
    void doFilterInternalTest() throws Exception {
        // Create a JWT token using the JWT_SECRET
        String token = jwtProvider.generateToken(authentication);
        assertNotNull(token);
        // Create a mock HTTP request and add the JWT token as the "Authorization" header
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", "Bearer " + token);
        // Create a mock HTTP response
        MockHttpServletResponse response = new MockHttpServletResponse();
        // Use reflection to access the `doFilterInternal` method of the `JWTAuthenticationFilter` class
        Method doFilterInternal = JWTAuthenticationFilter.class.getDeclaredMethod("doFilterInternal", HttpServletRequest.class, HttpServletResponse.class, FilterChain.class);
        // Make the `doFilterInternal` method accessible so that it can be invoked using reflection
        doFilterInternal.setAccessible(true);
        // Invoke the `doFilterInternal` method using reflection
        doFilterInternal.invoke(jwtAuthenticationFilter, request, response, new MockFilterChain());
        // Get the `Authentication` object from the security context
        Authentication internalAuth = SecurityContextHolder.getContext().getAuthentication();
        // Verify that the authentication is not null and that the user is authenticated
        assertNotNull(internalAuth);
        assertTrue(internalAuth.isAuthenticated());
        // Verify that the name of the authenticated user is "user"
        assertEquals("username", internalAuth.getName());
    }

    @AfterEach
    void drop() {
        userService.delete(user);
    }
}
