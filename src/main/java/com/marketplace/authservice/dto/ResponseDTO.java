package com.marketplace.authservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * DTO объект, использующийся для возврата данных с информационным сообщением.
 */
@AllArgsConstructor
@Data
public class ResponseDTO {
    private String message;
}
