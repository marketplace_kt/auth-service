package com.marketplace.authservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * DTO объект, использующийся для возврата данных залогиненного пользователя.
 */

@AllArgsConstructor
@Data
public class AuthResponseDto {
    private String accessToken;
}
