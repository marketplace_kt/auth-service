package com.marketplace.authservice.util.exceptions;

public class AccessDeniedException extends RuntimeException{
    
    public AccessDeniedException(String msg) {
        super(msg);
    }
}