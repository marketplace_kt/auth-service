package com.marketplace.authservice.config.jwt;

import com.marketplace.authservice.config.security.SecurityConstants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.NoArgsConstructor;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Класс для генерации, парсинга и валидации JWT токенов.
 */
@Component
@NoArgsConstructor
public class JWTProvider {

    /**
     * Метод генерирует JWT-токен на основе объекта аутентификации, который содержит учетные данные пользователя.
     * Этот метод использует конструктор Jwts для установки субъекта токена (имя пользователя), даты выдачи и срока действия,
     * а также подписывает токен секретным ключом, который хранится в классе SecurityConstants.
     */
    public String generateToken(Authentication authentication) {
        String username = authentication.getName();
        Date currentDate = new Date();
        Date expireDate = new Date(currentDate.getTime() + SecurityConstants.JWT_EXPIRATION);

        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(currentDate)
                .setExpiration(expireDate)
                .signWith(Keys.hmacShaKeyFor(SecurityConstants.JWT_SECRET), SignatureAlgorithm.HS512)
                .compact();
    }

    /**
     * https://auth0.com/docs/secure/tokens/json-web-tokens/json-web-token-claims - чтобы понять, что такое Claims
     * Метод берет сгенерированный токен и парсит содержащуюся информацию, возвращая оттуда поле "sub", которое содержит имя пользователя.
     */
    public String getUsernameFromJWT(String token) {
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(SecurityConstants.JWT_SECRET)
                .build()
                .parseClaimsJws(token)
                .getBody();
        return claims.getSubject();
    }

    /**
     * Метод парсит и валидирует корректность JWT токена. Возвращает true в случае успеха, иначе бросает эксепшн.
     */
    public boolean validateToken(String token) {
        try {
            Jwts.parserBuilder().setSigningKey(SecurityConstants.JWT_SECRET).build().parseClaimsJws(token);
            return true;
        } catch (Exception ex) {
            throw new AuthenticationCredentialsNotFoundException("JWT was expired or incorrect");
        }
    }
}
