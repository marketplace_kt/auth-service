package com.marketplace.authservice.config.jwt;

import com.marketplace.authservice.config.security.UserEntityDetailsService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

/**
 * Кастомный фильтр для JWT аутентификации, который будет использован как дополнительный фильтр в Spring Security конфиге.
 */

public class JWTAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JWTProvider jwtProvider;
    @Autowired
    private UserEntityDetailsService userEntityDetailsService;

    /**
     * Основной метод фильтра.
     * Сначала он извлекает токен JWT из заголовка запроса, затем проверяет наличие и валидность токена.
     * Если токен действителен, он извлекает имя пользователя из токена.
     * После этого он загружает данные пользователя.
     * Затем он создает экземпляр UsernamePasswordAuthenticationToken, используя данные пользователя, полномочия и устанавливает данные в токен аутентификации.
     * Наконец, помещает токен аутентификации в SecurityContext и вызывает метод doFilter цепочки фильтров, который позволяет продолжить запрос к следующему фильтру.
     */
    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull FilterChain filterChain)
            throws ServletException, IOException {
        try {
            String token = getJWTFromRequest(request);
            if (StringUtils.hasText(token) && jwtProvider.validateToken(token)) {
                String username = jwtProvider.getUsernameFromJWT(token);
                UserDetails userDetails = userEntityDetailsService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        filterChain.doFilter(request, response);
    }

    /**
     * Приватный метод класса, используемый в методе doFilterInternal для извлечения JWT токена из заголовка запроса.
     */
    private String getJWTFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }
}
