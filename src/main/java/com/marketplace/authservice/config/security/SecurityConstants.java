package com.marketplace.authservice.config.security;

import lombok.experimental.UtilityClass;

@UtilityClass
public class SecurityConstants {

    public static final Long JWT_EXPIRATION = 6000000L;

    public static final byte[] JWT_SECRET = "c2VjcmV0X3N0cmluZ18xMjM0NTY3ODkwXzEyMzQ1Njc4OTA=c2VjcmV0X3N0cmluZ18xMjM0NTY3ODkwXzEyMzQ1Njc4OTA".getBytes();
}
