package com.marketplace.authservice.config.security;

import com.marketplace.authservice.entity.RoleEnum;
import com.marketplace.authservice.entity.UserEntity;
import com.marketplace.authservice.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


@AllArgsConstructor
@Service
public class UserEntityDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found"));
        return new User(user.getUsername(), user.getPassword(), getAuthorities(user.getRole()));
    }

    public Collection<GrantedAuthority> getAuthorities(RoleEnum roles) {
        Collection<GrantedAuthority>  collectionAuthority = new ArrayList<>();
        collectionAuthority.add(new SimpleGrantedAuthority(roles.getAuthority()));
        return collectionAuthority;
    }
}

