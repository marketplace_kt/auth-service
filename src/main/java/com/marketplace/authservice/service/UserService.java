package com.marketplace.authservice.service;


import com.marketplace.authservice.dto.AuthRequestDto;
import com.marketplace.authservice.entity.UserEntity;

/** Интерфейс сервиса для работы с пользователем.
 */
public interface UserService {
    void register(AuthRequestDto registerDto);

    boolean existsByUsername(AuthRequestDto registerDto);

    void delete(UserEntity user);
}
