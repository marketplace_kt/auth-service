package com.marketplace.authservice.service;

import com.marketplace.authservice.dto.AuthRequestDto;
import com.marketplace.authservice.entity.UserEntity;
import com.marketplace.authservice.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void register(AuthRequestDto registerDto) {
        UserEntity user = new UserEntity();
        user.setUsername(registerDto.getUsername());
        user.setPassword(passwordEncoder.encode(registerDto.getPassword()));
        user.setRole(registerDto.getRole()); // TODO Исправить, сетить роль пользователя
        userRepository.save(user);
    }

    @Override
    public boolean existsByUsername(AuthRequestDto registerDto) {
        return userRepository.existsByUsername(registerDto.getUsername());
    }

    @Override
    @Transactional
    public void delete(UserEntity user) {
        userRepository.deleteByUsername(user.getUsername());
    }
}
