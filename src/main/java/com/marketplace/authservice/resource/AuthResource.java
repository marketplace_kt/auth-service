package com.marketplace.authservice.resource;

import com.marketplace.authservice.dto.AuthRequestDto;
import com.marketplace.authservice.dto.AuthResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


@RequestMapping("/api/auth")
public interface AuthResource {

    @PostMapping("/sign-in")
    @Operation(summary = "Sign-in to the account", description = "Provide a valid email and password to gain access to the account")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully signed in"),
            @ApiResponse(responseCode = "400", description = "Invalid email or password"),
            @ApiResponse(responseCode = "403", description = "Unauthorized access")
    })
    ResponseEntity<String> signIn(@RequestBody AuthRequestDto user);

    @Operation(summary = "Login to the account", description = "Provide a valid email and password to gain access to the account")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully logged in"),
            @ApiResponse(responseCode = "400", description = "Invalid email or password"),
            @ApiResponse(responseCode = "403", description = "Unauthorized access")
    })
    @PostMapping("/login")
    ResponseEntity<AuthResponseDto> login(@RequestBody AuthRequestDto loginDto);

}
